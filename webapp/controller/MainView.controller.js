sap.ui.define([
	"sap/ui/core/mvc/Controller"
], function (Controller) {
	"use strict";

	return Controller.extend("com.demo.s276.PurchaseOrderApp.controller.MainView", {
		onInit: function () {

		},
		onClickPO: function (oEvent) {
			var oApp = this.getView().getContent()[0];
			var sBindingPath = oEvent.getSource().getBindingContext().getPath();
			var oDetailsPage = oApp.getPages()[1].bindElement(sBindingPath);
			oApp.to(oDetailsPage.getId());
		},
		onNavButtonPress: function (oEvent) {
			var oApp = this.getView().getContent()[0];
			var oStartPage = oApp.getPages()[0];
			oApp.back(oStartPage.getId());
		}
	});
});