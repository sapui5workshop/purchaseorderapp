/* global QUnit*/

sap.ui.define([
	"sap/ui/test/Opa5",
	"com/demo/s276/PurchaseOrderApp/test/integration/pages/Common",
	"sap/ui/test/opaQunit",
	"com/demo/s276/PurchaseOrderApp/test/integration/pages/MainView",
	"com/demo/s276/PurchaseOrderApp/test/integration/navigationJourney"
], function (Opa5, Common) {
	"use strict";
	Opa5.extendConfig({
		arrangements: new Common(),
		viewNamespace: "com.demo.s276.PurchaseOrderApp.view.",
		autoWait: true
	});
});